# [algae](https://hex.pm/packages/algae)

![Hex.pm](https://img.shields.io/hexpm/dw/algae)
Bootstrapped algebraic data types for Elixir
